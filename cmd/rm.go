/*
Copyright © 2022 Shane Moore shane@shanemoore.me

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os/exec"
	"path/filepath"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/smoores/open-devcontainer/containers"
)

var force bool

// rmCmd represents the rm command
var rmCmd = &cobra.Command{
	Use:   "rm",
	Short: "Remove a workspace's devcontainer",
	Long: `Removes a devcontainer for a given workspace. If
the container is still running, this command will fail unless
the '--force' flag is passed.`,
	Run: func(cmd *cobra.Command, args []string) {
    fmt.Println("manifest", relativeManifestPath)
		manifestPath, _ := filepath.Abs(relativeManifestPath)
		manifest, err := readManifest(manifestPath)
		if err != nil {
			return
		}
		runtime := viper.GetString("runtime")
		containerConfig, err := containers.GetContainerConfig(manifest.ContainerName())
		if err != nil {
      log.Error().Err(err).Str("container", manifest.ContainerName()).Msg("Couldn't find container")
			return
		}

		containerRmArgs := []string{"container", "rm"}
		if force {
			containerRmArgs = append(containerRmArgs, "-f")
		}
		containerRmArgs = append(containerRmArgs, containerConfig.ID)
		out, err := exec.Command(runtime, containerRmArgs...).CombinedOutput()
		if err != nil {
      log.Error().Str("container", manifest.ContainerName()).Str("error", string(out)).Msg("Failed to remove container")
			return
		}

		err = KillPortForwarding(manifest)
		if err != nil {
      log.Error().Err(err).Msg("Failed to kill port forwarding daemons")
		}
	},
}

func init() {
	rootCmd.AddCommand(rmCmd)

	rmCmd.Flags().BoolVarP(&force, "force", "f", false, "Remove the container even if it's running.")
}
