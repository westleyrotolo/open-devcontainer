package containers

import (
	"encoding/json"
	"errors"
	"os/exec"
	"strings"

	"github.com/spf13/viper"
)

func GetContainerConfig(containerName string) (ContainerIDsAndLabels, error) {
	runtime := viper.GetString("runtime")
	containerJSONRaw, err := exec.Command(runtime, "inspect", containerName).Output()
	if err != nil {
		return ContainerIDsAndLabels{}, errors.New(string(containerJSONRaw))
	}
	containerJSONArray := []ContainerIDsAndLabels{}
	containerJSONRawTrimmed := []byte(strings.TrimSuffix(string(containerJSONRaw), "\n"))
	err = json.Unmarshal(containerJSONRawTrimmed, &containerJSONArray)
	if err != nil {
		return ContainerIDsAndLabels{}, err
	}
	containerJSON := containerJSONArray[0]
	return containerJSON, nil
}
