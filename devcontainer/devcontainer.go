/*
Copyright © 2022 Shane Moore shane@shanemoore.me

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package devcontainer

import (
	"crypto/sha256"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"github.com/docker/docker/api/types/strslice"
	"github.com/docker/go-connections/nat"
	"github.com/spf13/viper"
)

type Build struct {
	Dockerfile string             `json:"dockerfile"`
	Context    string             `json:"context"`
	Args       map[string]*string `json:"args"`
	Target     string             `json:"target"`
	CacheFrom  strslice.StrSlice  `json:"cacheFrom"`
}

type Protocol string

const (
	ProtocolUndefined Protocol = ""
	ProtocolHTTP      Protocol = "http"
	ProtocolHTTPS     Protocol = "https"
)

type OnAutoForward string

const (
	OnAutoForwardUndefined   OnAutoForward = ""
	OnAutoForwardNotify      OnAutoForward = "notify"
	OnAutoForwardOpenBrowser OnAutoForward = "openBrowser"
	OnAutoForwardOpenPreview OnAutoForward = "openPreview"
	OnAutoForwardSilent      OnAutoForward = "silent"
	OnAutoForwardIgnore      OnAutoForward = "ignore"
)

type PortAttributes struct {
	Label            string        `json:"label"`
	Protocol         Protocol      `json:"protocol"`
	OnAutoForward    OnAutoForward `json:"onAutoForward"`
	RequireLocalPort bool          `json:"requireLocalPort"`
	ElevateIfNeeded  bool          `json:"elevateIfNeeded"`
}

type UserEnvProbe string

const (
	UserEnvProbeUndefined             UserEnvProbe = ""
	UserEnvProbeNone                  UserEnvProbe = "none"
	UserEnvProbeInteractiveShell      UserEnvProbe = "interactiveShell"
	UserEnvProbeLoginShell            UserEnvProbe = "loginShell"
	UserEnvProbeLoginInteractiveShell UserEnvProbe = "loginInteractiveShell"
)

type ShutdownAction string

const (
	ShutdownActionUndefined     ShutdownAction = ""
	ShutdownActionNone          ShutdownAction = "none"
	ShutdownActionStopContainer ShutdownAction = "stopContainer"
	ShutdownActionStopCompose   ShutdownAction = "stopCompose"
)

type WaitFor string

const (
	WaitForUndefined            WaitFor = ""
	WaitForUpdateContentCommand WaitFor = "updateContentCommand"
	WaitForOnCreateCommand      WaitFor = "onCreateCommand"
)

type DevcontainerManifest struct {
	// Image or Dockerfile specific properties
	Image           string            `json:"image"`
	Dockerfile      string            `json:"dockerFile"`
	Context         string            `json:"context"`
	Build           Build             `json:"build"`
	AppPort         AppPort           `json:"appPort"`
	ContainerEnv    map[string]string `json:"containerEnv"`
	ContainerUser   string            `json:"containerUser"`
	MountStrings    []string          `json:"mounts"`
	WorkspaceMount  string            `json:"workspaceMount"`
	WorkspaceFolder string            `json:"workspaceFolder"`
	RunArgs         []string          `json:"runArgs"`
	// Docker compose specific properties
	DockerComposeFile strslice.StrSlice `json:"dockerComposeFile"`
	Service           string            `json:"service"`
	RunServices       string            `json:"runServices"`
	// General properties
	Name                 string                    `json:"name"`
	ForwardPorts         ForwardPorts              `json:"forwardPorts"`
	PortsAttributes      map[string]PortAttributes `json:"portsAttributes"`
	OtherPortsAttributes map[string]PortAttributes `json:"otherPortsAttributes"`
	RemoteEnv            map[string]string         `json:"remoteEnv"`
	RemoteUser           string                    `json:"remoteUser"`
	UpdateRemoteUserUID  bool                      `json:"updateRemoteUserUID"`
	UserEnvProbe         UserEnvProbe              `json:"userEnvProbe"`
	OverrideCommand      bool                      `json:"overrideCommand"`
	ShutdownAction       ShutdownAction            `json:"shutdownAction"`
	//features -- currently in preview
	// Lifecycle scripts
	InitializeCommand    CommandSlice `json:"initializeCommand"`
	OnCreateCommand      CommandSlice `json:"onCreateCommand"`
	UpdateContentCommand CommandSlice `json:"updateContentCommand"`
	PostCreateCommand    CommandSlice `json:"postCreateCommand"`
	PostStartCommand     CommandSlice `json:"postStartCommand"`
	PostAttachCommand    CommandSlice `json:"postAttachCommand"`
	WaitFor              WaitFor      `json:"waitFor"`
	// Metadata (manually assigned, not marshalled)
	ParentDirectory string
}

func (m DevcontainerManifest) ShortHash() string {
	sum := sha256.Sum256([]byte(m.ParentDirectory))
	fullHash := fmt.Sprintf("%x", sum)
	return fullHash[0:6]
}

func (m DevcontainerManifest) ContainerName() string {
	dirName := filepath.Base(filepath.Dir(m.ParentDirectory))
	filepathHash := m.ShortHash()
	return fmt.Sprintf("odc-%s-%s", dirName, filepathHash)
}

func (m DevcontainerManifest) SocatContainerName() string {
	return m.ContainerName() + "-socat"
}

func (m DevcontainerManifest) PodName() string {
	return m.ContainerName() + "-pod"
}

func (m DevcontainerManifest) NetworkName() string {
	return m.ContainerName() + "-net"
}

// Might want to make this an Unmarshaler?
func (m DevcontainerManifest) ContainerEnvSlice() []string {
	env := make([]string, 0, len(m.ContainerEnv))
	for k, v := range m.ContainerEnv {
		env = append(env, fmt.Sprintf("%s=%s", k, v))
	}
	env = append(env, "SSH_AUTH_SOCK=/tmp/ssh-agent.sock")
	return env
}

// Might want to make this an Unmarshaler?
func (m DevcontainerManifest) RemoteEnvSlice() []string {
	env := make([]string, 0, len(m.RemoteEnv))
	for k, v := range m.RemoteEnv {
		env = append(env, fmt.Sprintf("%s=%s", k, v))
	}
	return env
}

func (m DevcontainerManifest) ForwardedPorts() []uint16 {
	allRawPorts := append(m.AppPort, m.ForwardPorts...)
	forwardedPorts := make([]uint16, 0)
	for _, v := range allRawPorts {
		if !strings.Contains(v, ":") {
			i, err := strconv.ParseInt(v, 10, 16)
			if err == nil {
				forwardedPorts = append(forwardedPorts, uint16(i))
			}
		}
	}
	return forwardedPorts
}

func (m DevcontainerManifest) ContainerExecUser() string {
	if m.RemoteUser != "" {
		return m.RemoteUser
	}
	if m.ContainerUser != "" {
		return m.ContainerUser
	}
	return ""
}

func (m DevcontainerManifest) ImageTag() string {
	return m.ContainerName() + "-image"
}

func (m DevcontainerManifest) ImageBuildArgs() []string {
	args := make([]string, 0)
	args = append(args, "--tag", m.ImageTag())
	for buildArgName, buildArgValue := range m.Build.Args {
		args = append(args, "--build-arg", buildArgName+"="+*buildArgValue)
	}
	for _, cacheFrom := range m.Build.CacheFrom {
		args = append(args, "--cache-from", cacheFrom)
	}
	if m.Build.Target != "" {
		args = append(args, "--target", m.Build.Target)
	}

	absBuildContext := m.AbsDockerBuildContext()

	args = append(args, "-f", m.AbsDockerfile())
	args = append(args, absBuildContext)

	return args
}

func portableRunArgs(runArgs []string) []string {
	runtime := viper.GetString("runtime")
	portable := make([]string, 0)
	hasUserNs := false
	for _, arg := range runArgs {
		if arg == "--userns=keep-id" {
			hasUserNs = true
			if runtime == "podman" {
				portable = append(portable, arg)
			}
		} else {
			portable = append(portable, arg)
		}
	}
	if !hasUserNs && runtime == "podman" {
		portable = append(portable, "--userns=keep-id")
	}
	return portable
}

func (m DevcontainerManifest) ContainerCreateArgs(imageID string, workingDir string) []string {
	args := make([]string, 0)
	if m.ContainerUser != "" {
		args = append(args, "--user", m.ContainerUser)
	}
	for _, port := range m.publishedPorts() {
		args = append(args, "--publish", port)
	}
	for _, envVar := range m.ContainerEnvSlice() {
		args = append(args, "--env", envVar)
	}
	for _, mount := range m.Mounts(workingDir) {
		args = append(args, "--mount", mount)
	}
	runtime := viper.GetString("runtime")
	if runtime == "docker" {
		args = append(args, "--add-host", "host.docker.internal:host-gateway")
	}
	args = append(args, "--name", m.ContainerName())
	args = append(args, "--workdir", workingDir)
	args = append(args, portableRunArgs(m.RunArgs)...)
	args = append(args, imageID)
	args = append(args, m.Command()...)
	return args
}

func (m DevcontainerManifest) publishedPorts() []string {
	allRawPorts := append(m.AppPort, m.ForwardPorts...)
	publishedPorts := make([]string, 0)
	for _, v := range allRawPorts {
		if strings.Contains(v, ":") {
			publishedPorts = append(publishedPorts, v)
		}
	}
	return publishedPorts
}

func (m DevcontainerManifest) ExposedPorts() nat.PortSet {
	exposedPorts, _, _ := nat.ParsePortSpecs(m.publishedPorts())
	return exposedPorts
}

func (m DevcontainerManifest) PortBindings() nat.PortMap {
	_, portBindings, _ := nat.ParsePortSpecs(m.publishedPorts())
	return portBindings
}

func (m DevcontainerManifest) AbsDockerfile() string {
	if m.Dockerfile != "" {
		return filepath.Join(m.ParentDirectory, m.Dockerfile)
	}
	if m.Build.Dockerfile != "" {
		return filepath.Join(m.ParentDirectory, m.Build.Dockerfile)
	}
	return filepath.Join(m.ParentDirectory, "Dockerfile")
}

func (m DevcontainerManifest) AbsDockerBuildContext() string {
	if m.Build.Context != "" {
		return filepath.Join(m.ParentDirectory, m.Build.Context)
	}
	if m.Context != "" {
		return filepath.Join(m.ParentDirectory, m.Context)
	}
	return filepath.Join(m.ParentDirectory, "..")
}

func (m DevcontainerManifest) Command() []string {
	if m.OverrideCommand || m.DockerComposeFile == nil {
		return []string{"/bin/sh", "-c", "while sleep 1000; do :; done"}
	}
	return nil
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func (m DevcontainerManifest) GpgMounts() []string {
	runtime := viper.GetString("runtime")
	gpgMounts := make([]string, 0)
	gpgPubring := fmt.Sprintf("%s/.gnupg/pubring.kbx", os.Getenv("HOME"))
	gpgTrustdb := fmt.Sprintf("%s/.gnupg/trustdb.gpg", os.Getenv("HOME"))
	if fileExists(gpgPubring) {
		mount := fmt.Sprintf("type=bind,source=%s,target=%s",
			gpgPubring,
			fmt.Sprintf("/home/%s/.gnupg/pubring.kbx", m.RemoteUser),
		)
		if runtime == "podman" {
			mount = mount + ",Z"
		}
		gpgMounts = append(gpgMounts, mount)
	}
	if fileExists(gpgTrustdb) {
		mount := fmt.Sprintf("type=bind,source=%s,target=%s",
			gpgTrustdb,
			fmt.Sprintf("/home/%s/.gnupg/trustdb.gpg", m.RemoteUser),
		)
		if runtime == "podman" {
			mount = mount + ",Z"
		}
		gpgMounts = append(gpgMounts, mount)
	}
	return gpgMounts
}

func (m DevcontainerManifest) SshMounts() []string {
	runtime := viper.GetString("runtime")
	sshMounts := make([]string, 0)
	sshKnownHosts := fmt.Sprintf("%s/.ssh/known_hosts", os.Getenv("HOME"))
	sshConfig := fmt.Sprintf("%s/.ssh/config", os.Getenv("HOME"))
	if fileExists(sshKnownHosts) {
		mount := fmt.Sprintf("type=bind,source=%s,target=%s",
			sshKnownHosts,
			fmt.Sprintf("/home/%s/.ssh/known_hosts", m.RemoteUser),
		)
		if runtime == "podman" {
			mount = mount + ",Z"
		}
		sshMounts = append(sshMounts, mount)
	}
	if fileExists(sshConfig) {
		mount := fmt.Sprintf("type=bind,source=%s,target=%s",
			sshConfig,
			fmt.Sprintf("/home/%s/.ssh/config", m.RemoteUser),
		)
		if runtime == "podman" {
			mount = mount + ",Z"
		}
		sshMounts = append(sshMounts, mount)
	}
	return sshMounts
}

func (manifest DevcontainerManifest) processVariableInterpolation(raw string, workingDir string) string {
	// TODO: Support containerEnv, and do this for environment, too
	localEnvRegexp, _ := regexp.Compile(`\$\{localEnv:(\S*)\}`)
	interpolated := strings.ReplaceAll(raw, "${localWorkspaceFolder}", manifest.AbsDockerBuildContext())
	interpolated = strings.ReplaceAll(interpolated, "${containerWorkspaceFolder}", workingDir)
	interpolated = strings.ReplaceAll(interpolated, "${localWorkspaceFolderBasename}", filepath.Base(workingDir))
	localEnvSubmatches := localEnvRegexp.FindAllStringSubmatch(interpolated, -1)
	for _, submatch := range localEnvSubmatches {
		for i, match := range submatch {
			if i == 0 {
				continue
			}
			envValue := os.Getenv(match)
			interpolated = strings.Replace(interpolated, "${localEnv:"+match+"}", envValue, 1)
		}
	}
	return interpolated
}

func portableMounts(mounts []string) []string {
	runtime := viper.GetString("runtime")
	portable := make([]string, 0)
	for _, mount := range mounts {
		mountParts := strings.Split(mount, ",")
		portableParts := make([]string, 0)
		hasZ := false
		for _, part := range mountParts {
			if part == "Z" {
				hasZ = true
				if runtime == "podman" {
					portableParts = append(portableParts, part)
				}
			} else {
        portableParts = append(portableParts, part)
      }
		}
		if runtime == "podman" && !hasZ {
			portableParts = append(portableParts, "Z")
		}
		portable = append(portable, strings.Join(portableParts, ","))
	}
	return portable
}

func (manifest DevcontainerManifest) Mounts(workingDir string) []string {
	mounts := make([]string, 0)

	for _, v := range manifest.MountStrings {
		mounts = append(mounts, manifest.processVariableInterpolation(v, workingDir))
	}
	if manifest.WorkspaceMount != "" {
		mounts = append(mounts, manifest.processVariableInterpolation(manifest.WorkspaceMount, workingDir))
	} else {
		mounts = append(mounts, fmt.Sprintf("type=bind,source=%s,target=%s,consistency=cached", manifest.AbsDockerBuildContext(), workingDir))
	}
	mounts = append(mounts, manifest.GpgMounts()...)
	mounts = append(mounts, manifest.SshMounts()...)
	mounts = portableMounts(mounts)
	return mounts
}

func (m DevcontainerManifest) WorkingDir(sublimeTextCompat bool) string {
	if sublimeTextCompat {
		return m.AbsDockerBuildContext()
	}
	if m.WorkspaceFolder != "" {
		return m.WorkspaceFolder
	}
	return fmt.Sprintf("/workspaces/%s", filepath.Base(m.AbsDockerBuildContext()))
}
